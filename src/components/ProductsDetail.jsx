import React, { useContext } from 'react'
import { useParams } from 'react-router-dom'
import useAxios from "../hooks/useAxios";
import "../App.css"
import { BContext } from '../context/BasketContext';
function ProductsDetail() {
  const {basket,setBasket}=useContext(BContext);
  const addBasket = (product) => {
    if (basket.indexOf(product) !== -1) return;
    setBasket([...basket, product])
  }
  const { id } = useParams();
  const { res,loader } = useAxios("/products");
  return <div>
    {
      res.map((item, index) => {
        if (item.id == id) {
          return loader ? (<div className="spinner-border text-primary" role="status" >
            <span className="visually-hidden">Loading...</span>
          </div>):( <div key={index} className='d-flex align-items-center justify-content-center pt-5'>
            {<div className='d-flex bg-body-tertiary px-5 align-items-center justify-content-center rounded rounded-2 my-3 shadow p-3 mb-5 bg-body-tertiary rounded w-50 mt-5'>
              <div className='mx-5' height={"300px"}>
                <img src={item.thumbnail} className='images' width={"300px"} height={"300px"}/>
              </div>
              <div className='mx-5'>
                  <p className='fw-bold'>{item.title}</p>

                <div className='d-flex'>
                   <p className='text-decoration-line-through brand text-secondary'>
                    ${item.price}
                  </p>
                  <p className=' fw-bold border-dark text-dark mx-3 border-bottom'>${(item.discountPercentage * item.price / 100).toFixed(2)}</p>
                  <p>(%Off)</p>
                </div>
                 
                  <button className='productBtn px-3' onClick={()=>addBasket(item)}>Add to Cart</button>
              </div>
            </div>
            }
          </div>)
        }
      })
    }
  </div>

}

export default ProductsDetail