import React, { useContext, useState } from 'react'
import "../App.css"
import { Link} from 'react-router-dom'
import { BContext } from '../context/BasketContext'
import { toast } from 'react-toastify'
function ProductsList({ item}) {
  const {basket,setBasket,setDoubleProduct}=useContext(BContext);
  const addBasket=(product)=>{
    if(basket.indexOf(product)!==-1) return;
    setBasket([...basket, product])
    // else{
    //   if(basket?.find([product])){
    //     setDoubleProduct(true)
    //   }else{
    //     setBasket([...basket,product])
    //   }
    // }
    toast.success("Added to cart")
  }
  return (
  
    <div className='col'>
      <div className='row mx-1 mb-1 p-2' >
        <div className='bg-body-tertiary rounded-3 py-3 shadow p-3 mb-5 bg-body-tertiary '>
        <div className='' height={"250px"}>
          <img src={item.thumbnail} alt={item.title} height={"250px"} width={"250px"} className='images rounded rounded-1' />
        </div>
        <div className='d-flex px-5z justify-content-between flex-column'>
            <Link to={"products/" + `${item.id}`} className='text-decoration-none border-bottom pb-2'><span className='brand_title fw-normal text-dark'>Brand:</span> <span className='fw-bold brand'>{item.brand.substring(0,8)}</span> </Link> <br/>
          <Link to={"products/"+ `${item.id}`} className='fw-bold text-decoration-none product_title'>{item.title.substring(0,8)}</Link>
              <Link to={"products/" + `${item.id}`} className='fw-medium text-danger text-decoration-none mb-4 d-flex align-items-center justify-content-center'>
                <p className='text-decoration-line-through brand text-secondary'>
                ${item.price}
                </p>
                <p className='mx-3 fw-bold border-bottom border-dark text-dark'>${(item.discountPercentage*item.price/100).toFixed(2)}</p>
                <p>(%Off)</p>
              </Link>
        </div>
        <div className='d-flex justify-content-center align-items-center'>
          <button className=' fw-bold d-flex productBtn' onClick={()=>addBasket(item)} >Add to Cart</button>
        </div>
      </div>
      </div>
    </div>

  )
}

export default ProductsList;