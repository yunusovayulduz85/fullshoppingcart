import React, { useContext, useState } from 'react'
import "../App.css"
import { BContext } from '../context/BasketContext';
let totalPrice = 0;
function Basket({ item, removeBasketProduct }) {
  const {doubleProduct}=useContext(BContext)
  sessionStorage.setItem("basketProduct", item);
  const [counter, setCounter] = useState(1);
  const Added = () => {
    setCounter(prev => prev += 1)
  }
  const Separate = () => {
    setCounter(prev => prev != 1 ? (prev -= 1) : (prev))
  }
  {doubleProduct && Added()}
  return (
     <div className = 'pt-5'>
      <div className = 'd-flex bg-body-tertiary px-5 align-items-center justify-content-between rounded rounded-2  shadow p-5 ps-0 bg-body-tertiary rounded'>
        <div className = 'pt-5'></div >
        <div className='' height={"300px"}>
          <img src={item.thumbnail} width={"300px"} height={"300pc"} className='images' />
        </div>
        <div>
          <p className='fw-bold text-primary-emphasis fs-5'>{item.title}</p>
          <p className='fw-bold text-danger'>${item.price}</p>
        </div>
        <div className='d-flex flex-column justify-content-center'>
          <button className='btn btn-success' onClick={Added}>+</button>
          <p>{counter}</p>
          <button className='btn btn-danger' onClick={Separate}>-</button>
        </div>
        <div>
          <button className='btn btn-secondary' onClick={() => removeBasketProduct(item.id)}>Remove</button>
        </div>
      </div >
    <div className='d-flex justify-content-center align-items-center totalPrice w-50'>
    </div>
    </div >
   
  )
}

export default Basket