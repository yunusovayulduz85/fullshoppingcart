import React, { useContext } from 'react'
import { BContext } from '../context/BasketContext'
import { useNavigate } from 'react-router-dom'

function PrivateAuthRoute({children}) {
    const {userName}=useContext(BContext);
    const navigation=useNavigate();
     {
    if(userName){
        return <>{children}</>
    }else{
        alert("File not found!");
        navigation("/");
    }
  }
}

export default PrivateAuthRoute