import React, { useContext } from 'react';
import "../App.css"
import { BContext } from '../context/BasketContext';
import "../App.css"
import { toast } from 'react-toastify';
function CategoryCart({item}) {
    const {basket,setBasket}=useContext(BContext);
    const addBasket = (product) => {
        if (basket.indexOf(product) !== -1) return;
      setBasket([...basket, product])
      toast.success("Added to cart");
    }
  return (
      <div className='col mt-5 pt-2'>
          <div className='row mx-1 p-2'>
              <div className='bg-body-tertiary rounded py-2 shadow  bg-body-tertiary rounded d-flex align-items-center justify-content-center'>
                  <div className='' height={"300px"}>
                      <img src={item.thumbnail} alt={item.title} height={"300px"} width={"300px"} className='images' />
                  </div>
                  <div className='mx-3'>
                    <p>{item.title}</p>
                      <p className='text-decoration-line-through brand text-secondary'>
                          ${item.price}
                      </p>
                      <p className='mx-3 fw-bold border-dark text-dark'>${(item.discountPercentage * item.price / 100).toFixed(2)}</p>
                      <p>(%Off)</p>
                  <div className='d-flex align-items-center justify-content-center '>
                    <button className='productBtn px-3' onClick={()=>addBasket(item)}>Add to Cart</button>
                  </div>
                  </div>
                     
                  
              </div>
          </div>
      </div>
  )
}

export default CategoryCart