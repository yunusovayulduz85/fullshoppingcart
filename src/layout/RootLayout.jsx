import React, { useContext } from 'react'
import "../App.css"
import { Link, Outlet } from 'react-router-dom'
import Shopping from "../assets/cart3.svg";
import { BContext } from '../context/BasketContext';
import useAxios from '../hooks/useAxios';
function RootLayout() {
  const {basket}=useContext(BContext);
  return (
    <div>
      <nav className="navbar navbar-expand-lg w-100 nav mt-0 position-sticky">
    
        <div className="container-fluid " >
          <Link to={"/"} className="navbar-brand  fw-bold text-white" >Products</Link>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div className="navbar-nav">
              <ul className="navbar-nav" >
                <li className="nav-item">
                  <Link className="nav-link fw-medium text-white" aria-current="page" to={"/fragrances"}>Fragrances</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link  fw-medium text-white" to={"/groceries"}>Groceries</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link  fw-medium text-white" to={"/homeDecoration"}>HomeDecoration</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link  fw-medium text-white"  to={"/laptops"}>Laptops</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link  fw-medium text-white" to={"/skincare"}>Skincare</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link  fw-medium text-white" to={"/smartphones"}>Smartphones</Link>
                </li>
              </ul>
            </div>
          </div>
          <div className='d-flex align-items-center'>
            <ul className="navbar-nav mx-4">
              {/* <li className="nav-item">
                <Link className="nav-link  fw-medium text-white" to={"/dashboard"}>Admin</Link>
              </li> */}
              <li className="nav-item">
                <Link className="nav-link  fw-medium text-white" to={"/register"}>Register</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link  fw-medium text-white" to={"/login"}>Login</Link>
              </li>
             
            </ul>
          <div className='bg-white p-1 rounded'>
              <Link className='d-flex align-items-center justify-content-center text-decoration-none ' to={"/basket"}>
              <div className='d-flex align-items-center justify-content-center '>
                <img src={Shopping} width={"25px"}/>
                  <div className="nav px-1 rounded-circle d-flex align-items-center justify-content-center mb-3" >
                    <p className='text-decoration-none  fw-medium text-white m-0 '>{basket.length}</p>
                </div>
              </div>
            </Link>
          </div>
          </div>
        </div>
      </nav>
      <Outlet />
    </div>
  )
}

export default RootLayout