import React, { useContext } from "react"
import { BrowserRouter, Routes, Route } from "react-router-dom"
import RootLayout from '../layout/RootLayout'
import Home from '../pages/Home'
import ProductsLayout from '../layout/ProductsLayout'
import ProductsDetail from "../components/ProductsDetail";
import Fragrances from '../pages/Fragrances'
import Groceries from '../pages/Groceries'
import HomeDecoration from '../pages/HomeDecoration'
import Laptops from '../pages/Laptops'
import Skincare from '../pages/Skincare'
import Smartphones from '../pages/Smartphones'
import BasketList from '../pages/BasketList'
import { BContext } from "../context/BasketContext"
import Login from "../pages/Login"
import Register from "../pages/Register"
import Dashboard from "../pages/Dashboard"
import PrivateAuthRoute from "../components/PrivateAuthRoute"
import { ToastContainer } from "react-toastify"
function Router() {
  return (
    <div className="Router">
      <ToastContainer theme="colored"></ToastContainer>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<RootLayout />}>
            <Route index element={<Home />} />
            <Route path="products" element={<ProductsLayout />}>
              <Route index element={<Home />} />
              <Route path=':id' element={<ProductsDetail />} />
            </Route>
            <Route path='fragrances' element={<Fragrances />} />
            <Route path='groceries' element={<Groceries />} />
            <Route path='homeDecoration' element={<HomeDecoration />} />
            <Route path='laptops' element={<Laptops />} />
            <Route path='skincare' element={<Skincare />} />
            <Route path='smartphones' element={<Smartphones />} />
            <Route path='basket' element={<BasketList />} />
            <Route path="login" element={<Login />} />
            <Route path="register" element={<Register />} />
            {/* <Route path="dashboard" element={<PrivateAuthRoute> <Dashboard /></PrivateAuthRoute>} /> */}
            <Route path="dashboard" element={ <Dashboard />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  )
}

export default Router