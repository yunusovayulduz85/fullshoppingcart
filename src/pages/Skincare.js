import React, { useEffect, useState } from 'react'
import useAxios from "../hooks/useAxios";
import CategoryCart from '../components/CategoryCart';
function Skincare() {
  const { res,loader } = useAxios("products");
  const [products,setProducts]=useState([]);
  const skincare=(arr)=>{
    let skincareFilter=arr.filter((element)=>element.category==="skincare");
    setProducts(skincareFilter);
  }
  useEffect(()=>{
    skincare(res)
  },[res])
  return (
    <div className='container text-center'>
      <div className='row'>
        {
          loader ? (<div className="spinner-border text-primary" role="status">
            <span className="visually-hidden">Loading...</span>
          </div>):(products.map((item, i) =>
         <CategoryCart item={item} key={i} />) 
          )
        }
      </div>
    </div>
  )
}

export default Skincare;