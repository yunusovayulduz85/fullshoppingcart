import React, { useContext, useState } from 'react'
import { BContext } from '../context/BasketContext';
import { Link, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import "../App.css";
function Register() {
    const navigation = useNavigate();
    const [id, setId] = useState([]);
    const [name, setName] = useState("Yulduz Muxammatyunusova");
    const [passwordValue, setPasswordValue] = useState("");
    const [emailValue, setEmailValue] = useState("y@2505.com");
    const [phone, setPhone] = useState("1234567");
    const [country, setCountry] = useState("Uzbekistan");
    const [address, setAddress] = useState("Toshkent sh.");
    const [gender, setGender] = useState("female");

    const isValidate=()=>{
        let isProceed=true;
        let errorMassage="Please enter the value in "
        if(id===null || id===""){
            isProceed=false;
            errorMassage+="Username "
        }
        if(name===null || name===""){
            isProceed=false;
            errorMassage+="Full Name "
        }
        if(passwordValue===null || passwordValue===""){
            isProceed=false;
            errorMassage+="password "
        }
        if(emailValue===null || emailValue===""){
            isProceed=false;
            errorMassage+="email "
        }
        if(phone===null || phone===""){
            isProceed=false;
            errorMassage+="phone "
        }
        if(country===null || country===""){
            isProceed=false;
            errorMassage+="country "
        }
        if(address===null || address===""){
            isProceed=false;
            errorMassage+="address "
        }
        if(gender===null || gender===""){
            isProceed=false;
            errorMassage+="gender "
        }
        if(!isProceed){
            toast.warning(errorMassage)
        }else{
            if(/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/.test(emailValue)){
                // setUserName=id;
                // setPassword=passwordValue;
            }else{
                isProceed=false;
                toast.warning('Please Enter the valid email')
            }
        }
        return isProceed;
    }
    const handleSubmit=(e)=>{
        e.preventDefault();
        let regObj={id,name,passwordValue,emailValue,phone,country,address,gender};
        if(isValidate()){
            fetch("http://localhost:8000/user",{
            method:"POST",
            headers:{'content-type':'application/json'},
            body:JSON.stringify(regObj)
        }).then((res)=>{
            toast.success("Registered Successfully");
            navigation("/login")
        }).catch((err)=>{
            toast.error("Failed : " + err.massage)
        })}
    }
    return (
        <div className='pt-5'>
            <div className='pt-5'></div>
            <div className='offset-lg-3 col-lg-6'>
                <form className='container w-100' onSubmit={handleSubmit}>
                    <div className='card'>
                        <div className='card-header'>
                            <h1 className='text-center register'>User Registeration</h1>
                        </div>
                        <div className='card-body'>
                            <div className='row'>
                                <div className='col-lg-6'>
                                    <div className='form-group'>
                                        <label className='register'>User Name <span className='errmsg'>*</span></label>
                                        <input value={id} type='text' className='form-control'onChange={(e)=>setId(e.target.value)} />
                                    </div>
                                </div>
                                <div className='col-lg-6'>
                                    <div className='form-group'>
                                        <label className='register'>Password <span className='errmsg'>*</span></label>
                                        <input  type='password' value={passwordValue} onChange={(e)=>setPasswordValue(e.target.value)} className='form-control' />
                                    </div>
                                </div>
                                <div className='col-lg-6'>
                                    <div className='form-group'>
                                        <label className='register'>Full Name <span className='errmsg'>*</span></label>
                                        <input type='text' value={name} className='form-control' onChange={(e)=>setName(e.target.value)} />
                                    </div>
                                </div>
                                <div className='col-lg-6'>
                                    <div className='form-group'>
                                        <label className='register'>Email<span className='errmsg'>*</span></label>
                                        <input value={emailValue} className='form-control' onChange={(e)=>setEmailValue(e.target.value)} />
                                    </div>
                                </div>
                                <div className='col-lg-6'>
                                    <div className='form-group'>
                                        <label className='register'>Phone<span className='errmsg'>*</span></label>
                                        <input type='number' value={phone}  onChange={(e)=>setPhone(e.target.value)} className='form-control' />
                                    </div>
                                </div>
                                <div className='col-lg-6'>
                                    <div className='form-group'>
                                        <label className='register'>Country<span className='errmsg'>*</span></label>
                                        <select className='form-control' value={country} onChange={(e)=>setCountry(e.target.value)}>
                                            <option value='Uzbekistan'>Uzbekistan</option>
                                            <option value='Turkey'>Turkiya</option>
                                            <option value='USA'>USA</option>
                                        </select>
                                    </div>
                                </div>
                                <div className='col-lg-12'>
                                    <div className='form-group'>
                                        <label className='register'>Address<span className='errmsg'>*</span></label>
                                       <textarea value={address} onChange={(e)=>setAddress(e.target.value)} className='form-control'></textarea>
                                    </div>
                                </div>
                                <div className='col-lg-6'>
                                    <div className='form-group'>
                                        <label className='register'>Gender</label>
                                        <br/> 
                                        <input type='radio' value="male" name='gender' className='app-check' checked={gender==="male"} onChange={(e)=>setGender(e.target.value)}/>
                                        <label>Male</label>
                                        <input type='radio' value="female" name='gender' className='app-check' checked={gender === "female"} onChange={(e) => setGender(e.target.value)} />
                                        <label>Female</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='card-footer d-flex justify-content-end'>
                            <button type='text' className='btn btn-primary mx-3 register'>Register</button>
                            <Link to={"/login"} className='btn btn-danger register'>Back</Link>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default Register