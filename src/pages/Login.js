import React, { useContext, useEffect, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom';
import { BContext } from '../context/BasketContext';
import { toast } from 'react-toastify';
import "../App.css"
function Login() {
    const navigation = useNavigate();
    const [name, setName] = useState([]);
    const [passwordValue, setPasswordValue] = useState([]);
    useEffect(() => {
        sessionStorage.clear();
    },[])
    const proceedLogin = (e) => {
        e.preventDefault();
        if (validate()) {
            fetch("http://localhost:8000/user/" + name).then((res) => {
                return res.json();
            }).then((resp) => {
                // console.log(resp);
                // console.log(Object.keys(resp));
                if (Object.keys(resp).length === 0) {
                    toast.error("Please Enter valid username!")
                } else {
                    if (resp.passwordValue == passwordValue) {
                       
                        sessionStorage.setItem("username", name)
                        sessionStorage.setItem("userRole", resp.role)
                       const userRole= sessionStorage.getItem("userRole")
                        if(userRole==="admin"){
                            navigation("/dashboard");
                            toast.success("Success!");
                        }
                        else{
                            toast.error("You aren't admin")
                            navigation("/")
                        }
                    } else {
                        toast.error("Please Enter valid credentials!")
                    }
                }
            }).catch((err) => {
                toast.error("Login Failed due to : " + err.massage)
            })

        }

    }
    const validate = () => {
        let result = true;
        if (name === "" || name === null) {
            result = false;
            toast.warning("Please Enter UserName")
        }
        if (passwordValue === "" || passwordValue === null) {
            result = false;
            toast.warning("Please Enter Password")
        }
        return result;
    }
    return (
        <div className='row  pt-5'>
            <div className='pt-5'> </div>
            <div className='offset-lg-3 col-lg-6'>
                <form onSubmit={proceedLogin} className='container'>
                    <div className='card'>
                        <div className='card-header'>
                            <h2 className='register'>User Login</h2>
                        </div>
                        <div className='card-body'>
                            <div className='form-group'>
                                <label className='register'>User Name <span className='errmsg'>*</span> </label>
                                <input className='form-control' value={name} onChange={(e) => setName(e.target.value)} type='text' />
                            </div>
                            <div className='form-group'>
                                <label className='register'>Password <span className='errmsg'>*</span> </label>
                                <input type='password' className='form-control' value={passwordValue} onChange={(e) => setPasswordValue(e.target.value)} />
                            </div>
                        </div>
                        <div className='card-footer'>
                            <button type='submit' className='btn btn-primary mx-3 register'>Login</button>
                            <Link className='btn btn-success register' to={"/register"}>New User</Link>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    )
}

export default Login