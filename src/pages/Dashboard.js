import React, { useContext, useEffect, useState } from 'react'
import useAxios from '../hooks/useAxios'
import { BContext } from '../context/BasketContext';
import { Link, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import Person from "../assets/person.svg"
import "../App.css"
function Dashboard() {
  const { res, loader } = useAxios("/products");
  //*Contextdan value qilib berib yuborilgan aynan tanlangan cartlarni ushlashimiz uchun ochilgan state va setState olindi 
  const { basket, setBasket } = useContext(BContext);
  //*biror yozilgan buyruq bajarilgandan keyin aytilgan urlga otib yuboradi (bosiladigan linklarsiz!)
  const navigation = useNavigate();
  //* edit funksiyasi uchun chiqadigan modalni o'chirish, chiqarish uchun o'zgarib turuvchi qiymat kerak edi shuning uchun quyidagi state ochildi :
  const [showModal, setShowModal] = useState(false);
  //*add funksiyasi uchun chiqadigan modalni o'chirish, chiqarish uchun quyidagi useState ochildi
  const [addProductModal, setAddProductModal] = useState(false);
  //*editfunksiyasi uchun modaldagi inputlarni qiymatlarini saqlash uchun ochilgan useStatelar :
  const [img, setImg] = useState("")
  const [title, setTitle] = useState("")
  const [price, setPrice] = useState("")
  const [category, setCategory] = useState("");
  const [brand, setBrand] = useState("")
  const [disCount, setDiscount] = useState("")
  //* edit funksiyasi uchun aynan tanlangan productni data larini vaqtinchaga saqlab turadigan useStatelar:
  const [id, setId] = useState("");
  const [changeableImg, setChangeableImg] = useState("");
  const [changeableTitle, setChangeableTitle] = useState("");
  const [changeablePrice, setChangeablePrice] = useState("");
  const [changeableCategory, setChangeableCategory] = useState("");
  const [changeableBrand, setChangeableBrand] = useState("");
  const [changeableDiscount, setChangeableDiscount] = useState("");
  //* Login.js da kiritilgan password to'g'ri bo'lsa bizni username sessionStorage ga saqlanadi pastda biz uni getItem bilan olayapmiz UI ga chizib qo'yish uchun
  let username = sessionStorage.getItem("username");
  //* add funksiaysi uchun input value larini saqlaydigan useState lar ochamiz :
  const [addId,setAddId]=useState("");
  const [addImg,setAddImg]=useState("");
  const [addTitle,setAddTitle]=useState("");
  const [addPrice,setAddPrice]=useState("");
  const [addCategory,setAddCategory]=useState("");
  const [addBrand,setAddBrand]=useState("");
  const [addDiscount,setAddDiscount]=useState("");

  useEffect(() => {
    if (username === "" || username === null) {
      navigation("/login")
    }
  }, [])
  const applyingChanges = () => {
    setShowModal(false);
    fetch(`http://localhost:9000/products/${id}`, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        title,
        price,
        brand,
        thumbnail: img,
        category,
        discountPercentage: disCount
      })
    })
      .then(res => res.json())
      .then((id) => setBasket(basket.filter((_) => _.id === id)));
    toast.success("edited!")
  }
  const EditedProduct = (objProduct) => {
    console.log("click Edit button!");
    setShowModal(true);
    setId(objProduct.id);
    setChangeableImg(objProduct.thumbnail);
    setChangeableTitle(objProduct.title);
    setChangeablePrice(objProduct.price);
    setChangeableCategory(objProduct.category);
    setChangeableBrand(objProduct.brand);
    setChangeableDiscount(objProduct.discountPercentage);

  }
  const deleteProduct = (id) => {
    fetch(`http://localhost:9000/products/${id}`, {
      method: 'DELETE',
    })
      .then(res => res.json())
      .then((id) => setBasket(basket.filter((_) => _.id === id)));
    toast.success("removed")
  }
  const Added = () => {
    setAddProductModal(true);
  }
  const addedProduct = () => {
    fetch('https://localhost:9000/products', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        id:addId,
        thumbnail:addImg,
        title:addTitle,
        price:addPrice,
        category:addCategory,
        brand:addBrand,
        discountPercentage:addDiscount
      })
    })
      .then(res => res.json())
      .then(console.log);
  }
  return (
    <div>
      <div className='nav  bg-warning-subtle d-flex justify-content-between px-5' style={{ position: "sticky"}}>
        <div className='d-flex'>
          <img className='pt-4 pb-2' src={Person} alt='Person' />
          <Link className='m-0 pt-4 mx-1 pb-2 product_title text-decoration-none text-dark'  to={"/dashboard"}>{username}</Link>
        </div>
        <Link className='m-0 pt-4 pb-2 px-5 text-decoration-none' style={{ float: "right" }} to={"/login"}>LogOut</Link>
      </div>
      <div className='d-flex justify-content-start my-2'>
        {/* <button className='btn btn-success' onClick={Added}>Add Product+</button> */}
      </div>

      <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Image</th>
            <th scope="col">title</th>
            <th scope="col">price</th>
            <th scope="col">category</th>
            <th scope="col">brand</th>
            <th scope="col">Discount</th>
          </tr>
        </thead>
        <tbody>
          {
            res?.map((item) => (
              <tr key={item.id}>
                {/* {localStorage.setItem("data",item)} */}
                <th scope="row">{item.id}</th>
                <td>
                  <img src={item.thumbnail} alt={item.title} width={"80px"} />
                </td>
                <td>{item.title}</td>
                <td>${item.price}</td>
                <td>{item.category}</td>
                <td>{item.brand}</td>
                <td>{item.discountPercentage} % OFF</td>
                <td>
                  <button className='btn btn-secondary' onClick={() => EditedProduct(item)}>Edit</button>
                  <button className='btn btn-danger mx-2' onClick={() => deleteProduct(item.id)}>Delete</button>
                </td>
              </tr>
            ))
          }

        </tbody>
      </table>
      //*edit qilish uchun modal :
      {showModal && <div className="wrapperModal"><div className='updateCart bg-warning-subtle rounded-4 px-5 py-3'>
        <div className='d-flex align-items-center justify-content-between'>
          <h2 className='text-center fw-bold fs-3 productId'>Product in id : {id}</h2>
          <button onClick={() => setShowModal(false)} className='close'>X</button>
        </div>
        <div className='d-flex align-items-center justify-content-between w-100'>
          <img src={changeableImg} className='changeableaImg' />
          <input value={img} onChange={(e) => setImg(e.target.value)} placeholder='Change image src..' className='form-control ms-5 w-75' /> <br />
        </div>
        <div className='d-flex align-items-center justify-content-between w-100 mt-3'>
          <p className='m-0 productId'>{changeableTitle}:</p>
          <input value={title} onChange={(e) => setTitle(e.target.value)} placeholder='Change title..' className='form-control ms-5 w-75' /> <br />
        </div>
        <div className='d-flex align-items-center justify-content-between w-100 '>
          <p className='m-0 productId me-5'>${changeablePrice}: </p>
          <input value={price} onChange={(e) => setPrice(e.target.value)} placeholder='Change price..' className='form-control ms-5 w-75' /> <br />
        </div>
        <div className='d-flex align-items-center mt-1'>
          <p className='m-0 productId me-2'>{changeableCategory}:</p>
          <input value={category} onChange={(e) => setCategory(e.target.value)} placeholder='Change category..' className='form-control ms-5 w-75' /> <br />
        </div>
        <div className='d-flex align-items-center my-1 justify-content-between'>
          <p className='m-0 productId me-4'>{changeableBrand}:</p>
          <input value={brand} onChange={(e) => setBrand(e.target.value)} placeholder='Change brand..' className='form-control ms-5 w-75' /> <br />
        </div>
        <div className='d-flex align-items-center justify-content-between'>
          <p className='m-0 productId me-5'>{changeableDiscount}(%OFF):</p>
          <input value={disCount} onChange={(e) => setDiscount(e.target.value)} placeholder='Change disCount..' className='form-control ms-4 w-75' /> <br />
        </div>
        <div className='d-flex justify-content-end pt-3 pb-3'>
          <button className='btn btn-danger' onClick={() => setShowModal(false)}>Back</button> |
          <button className='btn btn-success' onClick={applyingChanges}>Save Changes</button>
        </div>
      </div>
      </div>
      }
      //*edit funksiyasi uchun chiqadigan modalni backgroundi
      {
        showModal && <div className='shadow'>*</div>
      }
      //*add qilish uchun modal :
      {
        addProductModal && <div className="wrapperModal"><div className='updateCart bg-warning-subtle rounded-4 px-5 py-3'>
          <div className='d-flex align-items-center justify-content-between'>
            <h2 className='text-center fw-bold fs-3 productId text-center'> Add Product</h2>
            <button onClick={() => setAddProductModal(false)} className='close'>X</button>
          </div>
          <div className='d-flex align-items-center justify-content-between w-100'>
            <input value={addId} onChange={(e) => setAddId(e.target.value)} placeholder='Enter the id..' className='form-control ms-5 w-75' /> <br />
          </div>
          <div className='d-flex align-items-center justify-content-between w-100'>
            <input value={addImg} onChange={(e) => setAddImg(e.target.value)} placeholder='Enter the image src..' className='form-control ms-5 w-75' /> <br />
          </div>
          <div className='d-flex align-items-center justify-content-between w-100 my-1'>
            <input value={addTitle} onChange={(e) => setAddTitle(e.target.value)} placeholder='Enter the title..' className='form-control ms-5 w-75' /> <br />
          </div>
          <div className='d-flex align-items-center justify-content-between w-100 '>
            <input value={addPrice} onChange={(e) => setAddPrice(e.target.value)} placeholder='Enter the price..' className='form-control ms-5 w-75' /> <br />
          </div>
          <div className='d-flex align-items-center mt-1'>
            <input value={addCategory} onChange={(e) => setAddCategory(e.target.value)} placeholder='Enter the category..' className='form-control ms-5 w-75' /> <br />
          </div>
          <div className='d-flex align-items-center my-1 justify-content-between'>
            <input value={addBrand} onChange={(e) => setAddBrand(e.target.value)} placeholder='Enter the brand..' className='form-control ms-5 w-75' /> <br />
          </div>
          <div className='d-flex align-items-center justify-content-between'>
            <input value={addDiscount} onChange={(e) => setAddDiscount(e.target.value)} placeholder='Enter the disCount..' className='form-control ms-5 w-75' /> <br />
          </div>
          <div className='d-flex justify-content-end pt-3 pb-3'>
            <button className='btn btn-danger' onClick={() => setAddProductModal(false)}>Back</button> |
            <button className='btn btn-success' onClick={addedProduct}>Added</button>
          </div>
        </div>
        </div>
      }
      //*add funksiyasi uchun chiqadigan modalni backgroundi
      {
        addProductModal && <div className='shadow'>*</div>
      }

    </div>
  )
}

export default Dashboard;