import React, { useContext } from 'react'
import Basket from '../components/Basket';
import { BContext } from '../context/BasketContext';

function BasketList() {
  const {basket,setBasket}=useContext(BContext)
  const removeBasketProduct=(id)=>setBasket(basket.filter((_)=> _.id!==id))
  const Storage=()=>{
    sessionStorage.setItem("Basket_Product",[...basket])
  }
  return (
    <div>
      {
        basket?.map((item,i) =>
          <Basket item={item} key={i} removeBasketProduct={removeBasketProduct}/>
          
        )
      }
    </div>
  )
}

export default BasketList;