import React, { useEffect, useState } from 'react'
import useAxios from "../hooks/useAxios";
import CategoryCart from '../components/CategoryCart';
function HomeDecoration() {
  const { res,loader } = useAxios("products");
  const [products,setProducts]=useState([]);
  const homeDecoration=(res)=>{
    let home_Decoration=res.filter((element)=>element.category==="home-decoration");
    setProducts(home_Decoration);
  }
  useEffect(()=>{
    homeDecoration(res)
  },[res])
  return (
    <div className='container text-center'>
      <div className='row'>
        {
          loader ? (<div className="spinner-border text-primary" role="status">
            <span className="visually-hidden">Loading...</span>
          </div>):( products.map((item, i) =>
            <CategoryCart item={item} key={i}/>)
          )
        }
      </div>
    </div>
  )
}

export default HomeDecoration;