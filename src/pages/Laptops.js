import React, { useEffect, useState } from 'react'
import useAxios from "../hooks/useAxios";
import CategoryCart from '../components/CategoryCart';
function Laptops() {
  const { res,loader } = useAxios("products");
  const [products, setProducts] = useState([]);
  const laptops=(arr)=>{
    let LaptopsFilter=arr.filter((element)=>element.category==="laptops");
    setProducts(LaptopsFilter);
  }
  useEffect(()=>{
    laptops(res)
  },[res])
  return (
    <div className='container text-center'>
      <div className='row'>
        {
          loader ? (<div className="spinner-border text-primary" role="status">
            <span className="visually-hidden">Loading...</span>
          </div>): (products.map((item, i) =>
        <CategoryCart item={item} key={i} />) 
          )
        }
      </div>
    </div>
  )
}

export default Laptops;