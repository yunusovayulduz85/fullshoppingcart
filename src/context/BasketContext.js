import React, { createContext, useState } from 'react'
import useAxios from '../hooks/useAxios';
export const BContext = createContext();
const BasketContextProvider = ({ children }) => {
  const [basket, setBasket] = useState([]);
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [doubleProduct, setDoubleProduct] = useState(false)
  return (
    <BContext.Provider value={{ basket, setBasket, userName, setUserName, password, setPassword,doubleProduct,setDoubleProduct }}>
      <div>{children}</div>
    </BContext.Provider>
  )
}
export default BasketContextProvider;
